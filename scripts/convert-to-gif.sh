#!/bin/bash

INPUT=$1
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
TEMP=$(mktemp -d 2>/dev/null || mktemp -d -t 'firdt')
OUTPUT=$DIR/../dist/firdt.gif

ffmpeg -i $INPUT -vf "fps=30,split[s0][s1];[s0]palettegen[p];[s1][p]paletteuse,crop=380:380:2:0" -loop 0 $TEMP/firdt.gif
if [[ $? = 0 ]]; then
    rm $OUTPUT
    cp $TEMP/firdt.gif $OUTPUT
fi
rm -fr $TEMP
