Firdt!
======

Welcome to the Firdt! repository, the home of the Firdt! animated GIF.


In This Repository
------------------

- `src/firdt.svg`_ is an Inkscape SVG file. This uses Inkscape's specific markup and is the original source
- `src/firdt-optimised.svg`_ is a cut-down optimised version with just the standard SVG markup, exported from Inkscape
- `dist/firdt.gif`_ is the end result, after converting the animated SVG to GIF
- `scripts/convert-to-gif.sh`_ is a script that uses ffmpeg to convert a video of the animated SVG to GIF


Converting to GIF
-----------------

There is no easy way to convert an animated SVG to a GIF. These are the steps I took:

1. Open the SVG in Firefox, which animates it
2. Use a screen recorder to record the animation as a video
3. Use a video editor to cut the video to the right length
4. Use the ``convert-to-gif.sh`` script to convert the video to a GIF


Using the GIF
-------------

The GIF can be used directly from the repository, thanks to GitLab's raw URLs.


Markdown
^^^^^^^^

To use the GIF in Markdown, you can copy and paste the following code::

   ![](https://gitlab.com/superfly/firdt/-/raw/master/dist/firdt.gif)

reStructuredText
^^^^^^^^^^^^^^^^

To use the GIF in reStructuredText, you can copy and paste the following code::

   .. image:: https://gitlab.com/superfly/firdt/-/raw/master/dist/firdt.gif

.. _src/firdt.svg: src/firdt.svg
.. _src/firdt-optimised.svg: src/firdt-optimised.svg
.. _dist/firdt.gif: dist/firdt.gif
.. _scripts/convert-to-gif.sh: scripts/convert-to-gif.sh
